section .data
    hello_world db "Hello world!", 10
    hello_world_len  equ $ - hello_world
section .text
    global _start
    _start:
        mov eax, 1
        mov edi, 1
        mov esi, hello_world
        mov edx, hello_world_len
        syscall
        mov eax, 60
        mov edi, 0
        syscall
