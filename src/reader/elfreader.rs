use std::fs::File;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::mem;

use byteorder::{ReadBytesExt, ByteOrder, BigEndian, LittleEndian};
use num::NumCast;

use elf::elftypes::*;
use elf::elf32types::*;
use elf::elf64types::*;

use reader::elfreadererror::*;

pub const ELF_MAGIC: [u8; 4] = [0x7F, 'E' as u8, 'L' as u8, 'F' as u8];

trait ElfReader<ElfN_Addr: Elf_Data, ElfN_Off: Elf_Data, Elf_NWord: Elf_Data> {
  fn read_elf_addr<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<ElfN_Addr, ReaderError>;
  fn read_elf_off<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<ElfN_Off, ReaderError>;
  fn read_elf_nword<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<Elf_NWord, ReaderError>;

  fn read_elf_header<S: Read + Seek, E: ByteOrder>(src: &mut S, e_ident: [u8; EI_NIDENT]) -> Result<ElfN_Ehdr<ElfN_Addr, ElfN_Off>, ReaderError> {
    return Ok(ElfN_Ehdr::<ElfN_Addr, ElfN_Off> {
      e_ident:      e_ident,
      e_type:       try!(src.read_u16::<E>()),
      e_machine:    try!(src.read_u16::<E>()),
      e_version:    try!(src.read_u32::<E>()),
      e_entry:      try!(Self::read_elf_addr::<S, E>(src)),
      e_phoff:      try!(Self::read_elf_off::<S, E>(src)),
      e_shoff:      try!(Self::read_elf_off::<S, E>(src)),
      e_flags:      try!(src.read_u32::<E>()),
      e_ehsize:     try!(src.read_u16::<E>()),
      e_phentsize:  try!(src.read_u16::<E>()),
      e_phnum:      try!(src.read_u16::<E>()),
      e_shentsize:  try!(src.read_u16::<E>()),
      e_shnum:      try!(src.read_u16::<E>()),
      e_shstrndx:   try!(src.read_u16::<E>()),
    });
  }

  fn read_elf_shdr<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<ElfN_Shdr<ElfN_Addr, ElfN_Off, Elf_NWord>, ReaderError> {
    return Ok(ElfN_Shdr::<ElfN_Addr, ElfN_Off, Elf_NWord> {
       sh_name:       try!(src.read_u32::<E>()),
       sh_type:       try!(src.read_u32::<E>()),
       sh_flags:      try!(Self::read_elf_nword::<S, E>(src)),
       sh_addr:       try!(Self::read_elf_addr::<S, E>(src)),
       sh_offset:     try!(Self::read_elf_off::<S, E>(src)),
       sh_size:       try!(Self::read_elf_nword::<S, E>(src)),
       sh_link:       try!(src.read_u32::<E>()),
       sh_info:       try!(src.read_u32::<E>()),
       sh_addralign:  try!(Self::read_elf_nword::<S, E>(src)),
       sh_entsize:    try!(Self::read_elf_nword::<S, E>(src)),
    });
  }

  fn read_elf_rel<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<ElfN_Rel<ElfN_Addr>, ReaderError> {
    return Ok(ElfN_Rel::<ElfN_Addr> {
      r_offset: try!(Self::read_elf_addr::<S, E>(src)),
      r_info:   try!(src.read_u32::<E>()),
    });
  }

  fn read_elf_progbits<S: Read + Seek, E: ByteOrder>(src: &mut S, shdr: &ElfN_Shdr<ElfN_Addr, ElfN_Off, Elf_NWord>) -> Result<Elf_Progbits, ReaderError> {
    let size: usize = try!(NumCast::from(shdr.sh_size).ok_or(ELFConvertError));
    let mut data = vec![0; size];
    try!(src.read(&mut data));
    return Ok(Elf_Progbits{ data:  data });
  }

  fn read_elf_strtab<S: Read + Seek, E: ByteOrder>(src: &mut S, shdr: &ElfN_Shdr<ElfN_Addr, ElfN_Off, Elf_NWord>) -> Result<Elf_Strtab, ReaderError> {
    let size: usize = try!(NumCast::from(shdr.sh_size).ok_or(ELFConvertError));
    let mut data = vec![0; size];
    try!(src.read(&mut data));
    return Ok(Elf_Strtab{ data:  data });
  }

  fn read_elf_sym<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<ElfN_Sym<ElfN_Addr>, ReaderError> {
    return Ok(ElfN_Sym::<ElfN_Addr> {
      st_name:  try!(src.read_u32::<E>()),
      st_value: try!(Self::read_elf_addr::<S, E>(src)),
      st_size:  try!(src.read_u32::<E>()),
      st_info:  try!(src.read_u8()),
      st_other: try!(src.read_u8()),
      st_shndx: try!(src.read_u16::<E>()),
    });
  }

  fn read_elf_symtab<S: Read + Seek, E: ByteOrder>(src: &mut S, shdr: &ElfN_Shdr<ElfN_Addr, ElfN_Off, Elf_NWord>) -> Result<ElfN_Symtab<ElfN_Addr>, ReaderError> {
    let size: usize = try!(NumCast::from(shdr.sh_size).ok_or(ELFConvertError));
    let mut data = Vec::new();
    for _ in 0..(size / mem::size_of::<ElfN_Sym<ElfN_Addr>>()) {
      data.push(try!(Self::read_elf_sym::<S, E>(src)));
    }
    return Ok(ElfN_Symtab{ data:  data });
  }

  fn read_elf_rela<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<ElfN_Rela<ElfN_Addr>, ReaderError> {
    return Ok(ElfN_Rela::<ElfN_Addr> {
      r_offset: try!(Self::read_elf_addr::<S, E>(src)),
      r_info:   try!(src.read_u32::<E>()),
      r_addend: try!(src.read_i32::<E>()),
    });
  }

  fn read_elf_relas<S: Read + Seek, E: ByteOrder>(src: &mut S, shdr: &ElfN_Shdr<ElfN_Addr, ElfN_Off, Elf_NWord>) -> Result<ElfN_Relas<ElfN_Addr>, ReaderError> {
    let size: usize = try!(NumCast::from(shdr.sh_size).ok_or(ELFConvertError));
    let mut data = Vec::new();
    for _ in 0..(size / mem::size_of::<ElfN_Rela<ElfN_Addr>>()) {
      data.push(try!(Self::read_elf_rela::<S, E>(src)));
    }
    return Ok(ElfN_Relas{ data:  data });
  }

  fn read_elf_sec<S: Read + Seek, E: ByteOrder>(src: &mut S, shdr: &ElfN_Shdr<ElfN_Addr, ElfN_Off, Elf_NWord>) -> Result<ElfN_Sec<ElfN_Addr>, ReaderError> {
    let sec_type = ShType::from(shdr.sh_type);
    let sec_off: u64 = try!(NumCast::from(shdr.sh_offset).ok_or(ELFConvertError));
    try!(src.seek(SeekFrom::Start(sec_off)));

    return Ok(match sec_type {
      ShType::SHT_NULL     => ElfN_Sec::nullbits,
      ShType::SHT_NOBITS   => ElfN_Sec::nobits,
      ShType::SHT_PROGBITS => ElfN_Sec::progbits(try!(Self::read_elf_progbits::<S, E>(src, shdr))),
      ShType::SHT_STRTAB   => ElfN_Sec::strtab(try!(Self::read_elf_strtab::<S, E>(src, shdr))),
      ShType::SHT_SYMTAB   => ElfN_Sec::symtab(try!(Self::read_elf_symtab::<S, E>(src, shdr))),
      ShType::SHT_RELA     => ElfN_Sec::relas(try!(Self::read_elf_relas::<S, E>(src, shdr))),
      _ => return Err(ReaderError::SectionError(ELFUnknownSectionError { sec_type: sec_type })),
    })
/*
    } else if sec_type == ShType::SHT_NOBITS {
      return Ok(Elf_Sec::nobits);
    } else if sec_type == ShType::SHT_SYMTAB {
    } else if sec_type == ShType::SHT_REL {
      for _ in 0..sec_size / mem::size_of::<ElfN_Rel<ElfN_Addr>>() {
        try!(Self::read_elf_rel::<S, E>(src));
      }
    } else if sec_type == ShType::SHT_RELA {
    }*/

  }

  fn read_elf_object<S: Read + Seek, E: ByteOrder>(src: &mut S, e_ident: [u8; EI_NIDENT]) -> Result<ElfN_Object<ElfN_Addr, ElfN_Off, Elf_NWord>, ReaderError> {
    let elf_ehdr = try!(Self::read_elf_header::<S, E>(src, e_ident));

    let shoff: u64 = try!(NumCast::from(elf_ehdr.e_shoff).ok_or(ELFConvertError));
    try!(src.seek(SeekFrom::Start(shoff)));
    let mut shdr_table = Vec::new();
    for _ in 0..elf_ehdr.e_shnum {
      shdr_table.push(try!(Self::read_elf_shdr::<S, E>(src)));
    }

    let mut sec_table = Vec::new();
    for entry in &shdr_table {
      sec_table.push(try!(Self::read_elf_sec::<S, E>(src, entry)));
    }

    return Ok(ElfN_Object::<ElfN_Addr, ElfN_Off, Elf_NWord> {
      e_hdr:        elf_ehdr,
      e_shdr_table: shdr_table,
      e_sections:   sec_table,
    });
  }
}

struct Elf64Reader;
impl ElfReader<Elf64_Addr, Elf64_Off, Elf_Xword> for Elf64Reader {
  fn read_elf_addr<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<Elf64_Addr, ReaderError> {
    return src.read_u64::<E>().map_err(ReaderError::IOError);
  }

  fn read_elf_off<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<Elf64_Off, ReaderError> {
    return src.read_u64::<E>().map_err(ReaderError::IOError);
  }

  fn read_elf_nword<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<Elf_Xword, ReaderError> {
    return src.read_u64::<E>().map_err(ReaderError::IOError);
  }
}

struct Elf32Reader;
impl ElfReader<Elf32_Addr, Elf32_Off, Elf_Word> for Elf32Reader {
  fn read_elf_addr<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<Elf32_Addr, ReaderError> {
    return src.read_u32::<E>().map_err(ReaderError::IOError);
  }

  fn read_elf_off<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<Elf32_Off, ReaderError> {
    return src.read_u32::<E>().map_err(ReaderError::IOError);
  }

  fn read_elf_nword<S: Read + Seek, E: ByteOrder>(src: &mut S) -> Result<Elf_Word, ReaderError> {
    return src.read_u32::<E>().map_err(ReaderError::IOError);
  }
}

fn read_elf_endian_object<T: Read + Seek, E: ByteOrder>(src: &mut T, e_ident: [u8; EI_NIDENT]) -> Result<Elf_Object, ReaderError> {
  return match ElfClass::from(e_ident[EI_CLASS]) {
    ElfClass::ELFCLASS32 => Ok(Elf_Object::elf32(try!(Elf32Reader::read_elf_object::<T, E>(src, e_ident)))),
    ElfClass::ELFCLASS64 => Ok(Elf_Object::elf64(try!(Elf64Reader::read_elf_object::<T, E>(src, e_ident)))),
    _ => Err(ReaderError::FormatError(ELFFormatError)),
  };
}

fn read_elf_object<T: Read + Seek>(src: &mut T) -> Result<Elf_Object, ReaderError> {
  let mut e_ident = [0; EI_NIDENT];
  try!(src.read(&mut e_ident));

  if &e_ident[0..4] != ELF_MAGIC {
    return Err(ReaderError::FormatError(ELFFormatError));
  }

  return match ElfData::from(e_ident[EI_DATA]) {
    ElfData::ELFDATA2LSB => read_elf_endian_object::<T, LittleEndian>(src, e_ident),
    ElfData::ELFDATA2MSB => read_elf_endian_object::<T, BigEndian>(src, e_ident),
    _ => Err(ReaderError::FormatError(ELFFormatError)),
  }
}

pub fn read_from_file(file_path: &str) -> Result<Elf_Object, ReaderError> {
  let mut file = try!(File::open(file_path).map_err(ReaderError::IOError));
  return read_elf_object(&mut file);
}
