// FIXME: Tidy up error handling
use elf::elftypes::*;
use std::io::Error;

#[derive(Debug)]
pub struct ELFFormatError;

#[derive(Debug)]
pub struct ELFConvertError;

#[derive(Debug)]
pub struct ELFUnknownSectionError {
  pub sec_type: ShType,
}

#[derive(Debug)]
pub enum ReaderError {
  IOError(Error),
  FormatError(ELFFormatError),
  ConvertError(ELFConvertError),
  SectionError(ELFUnknownSectionError),
}

impl From<Error> for ReaderError {
  fn from(e: Error) -> ReaderError {
    return ReaderError::IOError(e);
  }
}

impl From<ELFConvertError> for ReaderError {
  fn from(e: ELFConvertError) -> ReaderError {
    return ReaderError::ConvertError(e);
  }
}

impl From<ELFUnknownSectionError> for ReaderError {
  fn from(e: ELFUnknownSectionError) -> ReaderError {
    return ReaderError::SectionError(e);
  }
}
