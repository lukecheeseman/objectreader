use elf::elftypes::*;

pub type Elf64_Addr     = u64;
pub type Elf64_Off      = u64;
pub type Elf64_Ehdr     = ElfN_Ehdr<Elf64_Addr, Elf64_Off>;
pub type Elf64_Phdr     = ElfN_Phdr<Elf64_Addr, Elf64_Off>;
pub type Elf64_Shdr     = ElfN_Shdr<Elf64_Addr, Elf64_Off, Elf_Xword>;
pub type Elf64_Sym      = ElfN_Sym<Elf64_Addr>;
pub type Elf64_Rel      = ElfN_Rel<Elf64_Addr>;
pub type Elf64_Rela     = ElfN_Rela<Elf64_Addr>;
pub type Elf64_Dyn      = ElfN_Dyn<Elf64_Addr, Elf_Word>;

pub type Elf64_Object   = ElfN_Object<Elf64_Addr, Elf64_Off, Elf_Xword>;
