use elf::elftypes::*;

pub type Elf32_Addr     = u32;
pub type Elf32_Off      = u32;
pub type Elf32_Ehdr     = ElfN_Ehdr<Elf32_Addr, Elf32_Off>;
pub type Elf32_Phdr     = ElfN_Phdr<Elf32_Addr, Elf32_Off>;
pub type Elf32_Shdr     = ElfN_Shdr<Elf32_Addr, Elf32_Off, Elf_Word>;
pub type Elf32_Sym      = ElfN_Sym<Elf32_Addr>;
pub type Elf32_Rel      = ElfN_Rel<Elf32_Addr>;
pub type Elf32_Rela     = ElfN_Rela<Elf32_Addr>;
pub type Elf32_Dyn      = ElfN_Dyn<Elf32_Addr, Elf_Word>;

pub type Elf32_Object   = ElfN_Object<Elf32_Addr, Elf32_Off, Elf_Word>;
