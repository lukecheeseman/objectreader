use elf::elf32types::Elf32_Object;
use elf::elf64types::Elf64_Object;

use num::NumCast;
use std::fmt;

pub type Elf_Section  = u16;
pub type Elf_Version  = u16;
pub type Elf_Byte     = u8;
pub type Elf_Half     = u16;
pub type Elf_Word     = u32;
pub type Elf_Xword    = u64;
pub type Elf_Sword    = i32;
pub type Elf_Sxword   = i64;

// This is a type alias for the generic constraints
pub trait Elf_Data: NumCast + Copy + fmt::Display + fmt::LowerHex {}
impl<T: NumCast + Copy + fmt::Display + fmt::LowerHex> Elf_Data for T {}

pub const EI_MAG0: usize = 0;
pub const EI_MAG1: usize = 1;
pub const EI_MAG2: usize = 2;
pub const EI_MAG3: usize = 3;
pub const EI_CLASS: usize = 4;
pub const EI_DATA: usize = 5;
pub const EI_VERSION: usize = 6;
pub const EI_OSABI: usize = 7;
pub const EI_ABIVERSION: usize = 8;
pub const EI_PAD: usize = 9;
pub const EI_NIDENT: usize = 16;

pub struct ElfN_Ehdr<ElfN_Addr, ElfN_Off> {
  pub  e_ident:      [Elf_Byte; EI_NIDENT],
  pub  e_type:       Elf_Half,
  pub  e_machine:    Elf_Half,
  pub  e_version:    Elf_Word,
  pub  e_entry:      ElfN_Addr,
  pub  e_phoff:      ElfN_Off,
  pub  e_shoff:      ElfN_Off,
  pub  e_flags:      Elf_Word,
  pub  e_ehsize:     Elf_Half,
  pub  e_phentsize:  Elf_Half,
  pub  e_phnum:      Elf_Half,
  pub  e_shentsize:  Elf_Half,
  pub  e_shnum:      Elf_Half,
  pub  e_shstrndx:   Elf_Half,
}

pub struct ElfN_Phdr<ElfN_Addr, ElfN_Off> {
  p_type:       u32,
  p_offset:     ElfN_Off,
  p_vaddr:      ElfN_Addr,
  p_paddr:      ElfN_Addr,
  p_filesz:     u32,
  p_memsz:      u32,
  p_flags:      u32,
  p_align:      u32,
}

pub struct ElfN_Shdr<ElfN_Addr, ElfN_Off, Elf_NWord> {
  pub sh_name:      Elf_Word,
  pub sh_type:      Elf_Word,
  pub sh_flags:     Elf_NWord,
  pub sh_addr:      ElfN_Addr,
  pub sh_offset:    ElfN_Off,
  pub sh_size:      Elf_NWord,
  pub sh_link:      Elf_Word,
  pub sh_info:      Elf_Word,
  pub sh_addralign: Elf_NWord,
  pub sh_entsize:   Elf_NWord,
}

pub struct ElfN_Sym<ElfN_Addr> {
  pub st_name:      Elf_Word,
  pub st_value:     ElfN_Addr,
  pub st_size:      Elf_Word,
  pub st_info:      Elf_Byte,
  pub st_other:     Elf_Byte,
  pub st_shndx:     Elf_Half,
}

pub struct ElfN_Rel<ElfN_Addr> {
  pub r_offset:     ElfN_Addr,
  pub r_info:       Elf_Word,
}

pub struct ElfN_Rela<ElfN_Addr> {
  pub r_offset:     ElfN_Addr,
  pub r_info:       Elf_Word,
  pub r_addend:     Elf_Sword,
}

enum ElfN_Dyn_un<ElfN_Addr, Elf_NWord>{
  d_val (Elf_NWord),
  d_ptr (ElfN_Addr),
}

pub struct ElfN_Dyn<ElfN_Addr, Elf_NWord> {
  d_tag:        Elf_Sword,
  d_un:         ElfN_Dyn_un<ElfN_Addr, Elf_NWord>,
}

pub struct Elf_Progbits {
  pub data:         Vec<u8>,
}

pub struct Elf_Strtab {
  pub data:         Vec<u8>,
}

pub struct ElfN_Symtab<ElfN_Addr> {
  pub data:         Vec<ElfN_Sym<ElfN_Addr>>,
}

pub struct ElfN_Relas<ElfN_Addr> {
  pub data:         Vec<ElfN_Rela<ElfN_Addr>>,
}

pub enum ElfN_Sec<ElfN_Addr> {
  nullbits,
  nobits,
  progbits(Elf_Progbits),
  strtab(Elf_Strtab),
  symtab(ElfN_Symtab<ElfN_Addr>),
  relas(ElfN_Relas<ElfN_Addr>),
}

pub struct ElfN_Object<ElfN_Addr, ElfN_Off, Elf_NWord> {
  pub e_hdr:        ElfN_Ehdr<ElfN_Addr, ElfN_Off>,
  pub e_shdr_table: Vec<ElfN_Shdr<ElfN_Addr, ElfN_Off, Elf_NWord>>,
  pub e_sections :  Vec<ElfN_Sec<ElfN_Addr>>,
}

pub enum Elf_Object {
 elf32(Elf32_Object),
 elf64(Elf64_Object),
}

pub enum ElfClass {
  ELFCLASSNONE,
  ELFCLASS32,
  ELFCLASS64,
}

pub enum ElfData {
  ELFDATANONE,
  ELFDATA2LSB,
  ELFDATA2MSB,
}

pub enum ElfVersion {
  EV_NONE,
  EV_CURRENT,
}

pub enum ElfOSABI {
  ELFOSABI_NONE = 0,
  ELFOSABI_HPUX = 1,
  ELFOSABI_NETBSD = 2,
  //ELFOSABI_GNU = 3,
  ELFOSABI_LINUX = 3,
  ELFOSABI_HURD = 4,
  ELFOSABI_SOLARIS = 6,
  ELFOSABI_AIX = 7,
  ELFOSABI_IRIX = 8,
  ELFOSABI_FREEBSD = 9,
  ELFOSABI_TRU64 = 10,
  ELFOSABI_MODESTO = 11,
  ELFOSABI_OPENBSD = 12,
  ELFOSABI_OPENVMS = 13,
  ELFOSABI_NSK = 14,
  ELFOSABI_AROS = 15,
  ELFOSABI_FENIXOS = 16,
  ELFOSABI_CLOUDABI = 17,
  ELFOSABI_C6000_ELFABI = 64,
  //ELFOSABI_AMDGPU_HSA = 64,
  ELFOSABI_C6000_LINUX = 65,
  ELFOSABI_ARM = 97,
  ELFOSABI_STANDALONE = 255,
}

pub enum ElfType {
  ET_NONE = 0,
  ET_REL  = 1,
  ET_EXEC = 2,
  ET_DYN  = 3,
  ET_CORE = 4,
}

// TODO: these should live elsewhere
impl From<Elf_Byte> for ElfClass {
  fn from(byte: Elf_Byte) -> ElfClass {
    return match byte {
      1 => ElfClass::ELFCLASS32,
      2 => ElfClass::ELFCLASS64,
      _ => ElfClass::ELFCLASSNONE,
    };
  }
}

impl From<Elf_Byte> for ElfData {
  fn from(byte: Elf_Byte) -> ElfData {
    return match byte {
      1 => ElfData::ELFDATA2LSB,
      2 => ElfData::ELFDATA2MSB,
      _ => ElfData::ELFDATANONE,
    };
  }
}

impl From<Elf_Byte> for ElfVersion {
  fn from(byte: Elf_Byte) -> ElfVersion {
    return match byte {
      1 => ElfVersion::EV_CURRENT,
      _ => ElfVersion::EV_NONE,
    };
  }
}

impl From<Elf_Byte> for ElfOSABI {
  fn from(byte: Elf_Byte) -> ElfOSABI {
    return match byte {
      0  => ElfOSABI::ELFOSABI_NONE,
      1  => ElfOSABI::ELFOSABI_HPUX,
      2  => ElfOSABI::ELFOSABI_NETBSD,
      3  => ElfOSABI::ELFOSABI_LINUX,
      4  => ElfOSABI::ELFOSABI_SOLARIS,
      5  => ElfOSABI::ELFOSABI_IRIX,
      6  => ElfOSABI::ELFOSABI_FREEBSD,
      7  => ElfOSABI::ELFOSABI_TRU64,
      8  => ElfOSABI::ELFOSABI_ARM,
      9  => ElfOSABI::ELFOSABI_STANDALONE,
      _  => ElfOSABI::ELFOSABI_NONE, // FIXME
    };
  }
}

impl From<Elf_Half> for ElfType {
  fn from(half: Elf_Half) -> ElfType {
    return match half {
      0 => ElfType::ET_REL,
      _ => ElfType::ET_NONE,
    };
  }
}

#[derive(Debug, PartialEq, PartialOrd)]
pub enum ShType {
  SHT_NULL,
  SHT_PROGBITS,
  SHT_SYMTAB,
  SHT_STRTAB,
  SHT_RELA,
  SHT_HASH,
  SHT_NOBITS,
  SHT_REL,
  SHT_SHLIB,
  SHT_DYNSYM,
  // TODO: others
}

impl From<Elf_Word> for ShType {
  fn from(word: Elf_Word) -> ShType {
    return match word {
      1 => ShType::SHT_PROGBITS,
      2 => ShType::SHT_SYMTAB,
      3 => ShType::SHT_STRTAB,
      4 => ShType::SHT_RELA,
      5 => ShType::SHT_HASH,
      6 => ShType::SHT_NOBITS,
      7 => ShType::SHT_REL,
      8 => ShType::SHT_SHLIB,
      9 => ShType::SHT_DYNSYM,
      _ => ShType::SHT_NULL,
    };
  }
}

// TODO: not quite
pub enum ShFlagsA {
  SHF_WRITE = 1,
  SHF_ALLOC = 2,
  SHF_EXECINSTR = 4,
  SHF_MASKPROC = 0xf0000000,
}

bitflags! { 
  pub flags ShFlags: u32 {
    const SHF_WRITE     = 0x00000001,
    const SHF_ALLOC     = 0x00000002,
    const SHF_EXECINSTR = 0x00000004,
    const SHF_MASKPROC  = 0xf0000000,
  }
}
