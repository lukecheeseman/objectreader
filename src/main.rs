extern crate getopts;
extern crate objreader;

use std::env;
use std::io::Write;

use getopts::Options;

use objreader::reader::elfreader;
use objreader::printer::elfprinter;

fn print_help(opts: Options) {
  let splat = format!("Usage: objreader FILE [options]");
  print!("{}", opts.usage(&splat));
}

fn main() {
  let args: Vec<String> = env::args().collect();

  let mut opts = Options::new();
  opts.optopt("o", "output", "set output file name", "NAME");
  opts.optflag("h", "help", "print this help menu");
  let matches = match opts.parse(&args[1..]) {
      Ok(m) => { m }
      Err(f) => { panic!(f.to_string()) }
  };

  if matches.opt_present("h") {
    print_help(opts);
    return;
  }

  if matches.free.is_empty() {
    let _ = writeln!(std::io::stderr(), "Error: expected input file");
  }

  let input = matches.free[0].clone();
  let result = elfreader::read_from_file(&input);
  match result {
    Ok(elf)   => elfprinter::print_elf_object(&elf),
    Err(e)  => println!("Failed to read {}: {:?}", input, e),
  }
}

