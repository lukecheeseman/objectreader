extern crate byteorder;
extern crate num;

#[macro_use]
extern crate bitflags;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}

#[allow(non_camel_case_types)]
pub mod reader {
  pub mod elfreader;
  pub mod elfreadererror;
}

#[allow(non_camel_case_types)]
pub mod printer {
  pub mod elfprinter;
}

#[allow(non_camel_case_types)]
#[allow(dead_code)]
pub mod elf {
  pub mod elftypes;
  pub mod elf32types;
  pub mod elf64types;
}
