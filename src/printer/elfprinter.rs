use std::fmt;
use std::str;

use elf::elftypes::*;
use elf::elf32types::*;
use elf::elf64types::*;

use num::ToPrimitive;

impl fmt::Display for ElfClass {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}",
      match self {
        &ElfClass::ELFCLASS32   => "ELF32",
        &ElfClass::ELFCLASS64   => "ELF64",
        _ => "ELFNONE",
      }
    )
  }
}

impl fmt::Display for ElfData {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}",
      match self {
        &ElfData::ELFDATA2LSB   => "2's complement, little endian",
        &ElfData::ELFDATA2MSB   => "2's complement, big endian",
        _ => "ELFNONE",
      }
    )
  }
}

impl fmt::Display for ElfVersion {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}",
      match self {
        &ElfVersion::EV_CURRENT => "1 (current)",
        _ => "0 (not current)",
      }
    )
  }
}

impl fmt::Display for ElfOSABI {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}",
      match self {
        &ElfOSABI::ELFOSABI_HPUX => "HP-UX ABI",
        &ElfOSABI::ELFOSABI_NETBSD => "NetBSD ABI",
        &ElfOSABI::ELFOSABI_LINUX => "Linux ABI",
        &ElfOSABI::ELFOSABI_SOLARIS => "Solaris ABI",
        &ElfOSABI::ELFOSABI_IRIX => "IRIX ABI",
        &ElfOSABI::ELFOSABI_FREEBSD => "FreeBSD ABI",
        &ElfOSABI::ELFOSABI_TRU64 => "TRU64 UNIX ABI",
        &ElfOSABI::ELFOSABI_ARM => "ARM architecture ABI",
        &ElfOSABI::ELFOSABI_STANDALONE => "Stand-alone (embedded) ABI",
        _ => "UNIX System V ABI",
      }
    )
  }
}

impl fmt::Display for ElfType {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}",
      match self {
        &ElfType::ET_REL  => "REL (Relocatable File)",
        &ElfType::ET_EXEC => "EXEC (Executable File)",
        &ElfType::ET_DYN  => "DYN (Shared Object File)",
        &ElfType::ET_CORE => "CORE (Core File)",
        _ => "NONE",
      }
    )
  }
}

impl fmt::Display for ShType {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}",
      match self {
        &ShType::SHT_PROGBITS => "PROGBITS",
        &ShType::SHT_SYMTAB => "SYMTAB",
        &ShType::SHT_STRTAB => "STRTAB",
        &ShType::SHT_RELA => "RELA",
        &ShType::SHT_HASH => "HASH",
        &ShType::SHT_NOBITS => "NOBITS",
        &ShType::SHT_REL => "REL",
        &ShType::SHT_SHLIB => "SHLIB",
        &ShType::SHT_DYNSYM => "DYNSYM",
        &ShType::SHT_NULL => "NULL",
      }
    )
  }
}

/*
// FIXME: nope, but i'm too lazy to fix this right now
impl fmt::Display for ShFlags {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}",
      match self {
        &ShFlags::SHF_WRITE => "W",
        &ShFlags::SHF_ALLOC => "A",
        &ShFlags::SHF_EXECINSTR => "X",
        &ShFlags::SHF_MASKPROC => "  ",
      }
    )
  }
}
*/

impl fmt::Display for ShFlags {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    if self.contains(SHF_WRITE) {
      try!(write!(f, "W"));
    }
    if self.contains(SHF_ALLOC) {
      try!(write!(f, "A"));
    }
    if self.contains(SHF_EXECINSTR) {
      try!(write!(f, "X"));
    }
    Ok(())
  }
}

trait ElfPrinter<ElfN_Addr: Elf_Data, ElfN_Off: Elf_Data, Elf_NWord: Elf_Data> {
  fn get_name(strings: &ElfN_Sec<ElfN_Addr>, index: usize) -> Result<&str, ()> {
    match strings {
      &ElfN_Sec::strtab(ref tab) => {
        let mut i = index;
        while i < tab.data.len() && tab.data[i] != 0 { i = i + 1; }
        return Ok(str::from_utf8(&tab.data[index..i]).unwrap());
      },
      _ => return Err(()),
    };
  }

  fn print_elf_header(e_hdr: &ElfN_Ehdr<ElfN_Addr, ElfN_Off>) {
    let e_ident = &e_hdr.e_ident;

    let mut e_ident_str = String::new();
    let mut first = true;
    for i in 0..e_ident.len() {
      if !first {
        e_ident_str.push(' ');
      }
      e_ident_str.push_str(&format!("{:02x}", e_ident[i]));
      first = false;
    }

    let e_class = ElfClass::from(e_ident[EI_CLASS]);
    let e_data = ElfData::from(e_ident[EI_DATA]);
    let e_version = ElfVersion::from(e_ident[EI_VERSION]);
    let e_osabi = ElfOSABI::from(e_ident[EI_OSABI]);
    let e_abiversion = e_ident[EI_ABIVERSION];
    let e_type = ElfType::from(e_hdr.e_type);

    println!("ELF Header:
    Magic:   {}
    Class:                             {}
    Data:                              {}
    Version:                           {}
    OS/ABI:                            {}
    ABI Version:                       {}
    Type:                              {} TODO
    Machine:                           TODO Advanced Micro Devices X86-64
    Version:                           0x{:x}
    Entry point address:               0x{:x}
    Start of program headers:          {} (bytes into file)
    Start of section headers:          {} (bytes into file)
    Flags:                             0x{:x}
    Size of this header:               {} (bytes)
    Size of program headers:           {} (bytes)
    Number of program headers:         {}
    Size of section headers:           {} (bytes)
    Number of section headers:         {}
    Section header string table index: {}\n",
    e_ident_str,
    e_class,
    e_data,
    e_version,
    e_osabi,
    e_abiversion,
    e_type,
    e_hdr.e_version,
    e_hdr.e_entry,
    e_hdr.e_phoff,
    e_hdr.e_shoff,
    e_hdr.e_flags,
    e_hdr.e_ehsize ,
    e_hdr.e_phentsize,
    e_hdr.e_phnum,
    e_hdr.e_shentsize,
    e_hdr.e_shnum,
    e_hdr.e_shstrndx);
  }

  fn print_elf_shdr_table(obj: &ElfN_Object<ElfN_Addr, ElfN_Off, Elf_NWord>) {
    let shdr_table = &obj.e_shdr_table;
    let strings = &obj.e_sections[obj.e_hdr.e_shstrndx as usize];
    println!("Section Headers:");
    println!("    [Nr] Name              Type             Address           Offset             Size              EntSize          Flags  Link  Info  Align");
    for (i, entry) in shdr_table.iter().enumerate() {
      let sh_type = match entry.sh_type.to_u32() {
        Some(t) => ShType::from(t),
        _ => ShType::from(0),
      };
      let sh_flags = match entry.sh_flags.to_u32() {
        Some(f) => ShFlags::from_bits(f),
        _ => ShFlags::from_bits(0 as u32),
      };
      let sh_name = match Self::get_name(strings, entry.sh_name as usize) {
        Ok(k) => k,
        _ => "<wat>",
      };
      println!("    [{:2}] {:16} {:16} {:016x} {:08x} {:016x} {:016x} {} {} {} {}",
        i,
        sh_name,
        sh_type, //ShType::from(2 as u32),//FIXME: entry.sh_type),
        entry.sh_addr,
        entry.sh_offset,
        entry.sh_size,
        entry.sh_entsize,
        sh_flags.unwrap(),//FIXME: entry.sh_flags),
        entry.sh_link,
        entry.sh_info,
        entry.sh_addralign);
    }
    println!("");
  }

  fn print_elf_symtab(symtab: &ElfN_Symtab<ElfN_Addr>) {
    println!("Symbol table <name> contains {} entries:", symtab.data.len());
    for (i, entry) in symtab.data.iter().enumerate() {
      println!("{}: {:x} {:x}",
        i,
        entry.st_value,
        entry.st_size,
      );
    }
  }

  fn print_elf_sections(obj: &ElfN_Object<ElfN_Addr, ElfN_Off, Elf_NWord>) {
    let sec_table = &obj.e_sections;
    for sec in sec_table {
      match sec {
        &ElfN_Sec::symtab(ref s) => Self::print_elf_symtab(s),
        _ => continue,
      }
    }
  }

  fn print_elf_object(obj: &ElfN_Object<ElfN_Addr, ElfN_Off, Elf_NWord>) {
    Self::print_elf_header(&obj.e_hdr);
    Self::print_elf_shdr_table(&obj);
    Self::print_elf_sections(&obj);
  }
}

struct Elf64Printer;
impl ElfPrinter<Elf64_Addr, Elf64_Off, Elf_Xword> for Elf64Printer {}

struct Elf32Printer;
impl ElfPrinter<Elf32_Addr, Elf32_Off, Elf_Word> for Elf32Printer {}

pub fn print_elf_object(obj: &Elf_Object) {
  match obj {
    &Elf_Object::elf64(ref e) => Elf64Printer::print_elf_object(&e),
    &Elf_Object::elf32(ref e) => Elf32Printer::print_elf_object(&e),
  }
}
